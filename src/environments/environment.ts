// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  token_expire: 14400000,
  url_register: 'http://trainherbackend.jaspertest.nl/api/register',
  url_login: 'http://trainherbackend.jaspertest.nl/api/login',
  url_user_info: 'http://trainherbackend.jaspertest.nl/api/user/info?token=',
  url_user_update: 'http://trainherbackend.jaspertest.nl/api/user/update?token=',
  url_calendar: 'http://trainherbackend.jaspertest.nl/api/calendar?token=',
  url_activity: 'http://trainherbackend.jaspertest.nl/api/activity/<id>?token='
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
