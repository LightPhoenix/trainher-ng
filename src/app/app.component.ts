import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  extraClass : string = ''

  constructor(private router: Router) {
      router.events.subscribe((val) => {
          if(this.router.url.indexOf('/calendar') > -1)
            this.extraClass = 'containerFull';
          else
            this.extraClass = '';
      });
  }
}
