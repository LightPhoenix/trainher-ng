import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { HttpErrorResponse } from '../../../../node_modules/@angular/common/http';
import { Observable } from '../../../../node_modules/rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    errors : string = null
    success : string = null

    loginFailed : boolean = false

    loginForm = new FormGroup({
        email: new FormControl("", [Validators.required, Validators.email]),
        password: new FormControl("", [Validators.required]),
    });

    constructor(private auth : AuthService, private router: Router) { }

    ngOnInit() {
    }

    get email(){
        return this.loginForm.get("email");
    }

    get password(){
        return this.loginForm.get("password");
    }

    doLogin() {
      this.auth.login(this.email.value, this.password.value).subscribe(
        data => this.handleLogin(data),
        error =>  this.handleError(error)
        );
    }

    handleError(err: HttpErrorResponse) {
        this.loginFailed = true;
    }

    handleLogin(data) {
        if (Boolean(data.success) == true) {
            this.auth.setToken(data.data.token);
            this.loginForm.reset();
            this.errors = null;

            this.success = data.message;
            this.loginForm.reset();

            this.auth.redirectAfterLogin();

        } else if (parseInt(data.status) == 401) {
            this.errors = data.error.error;
        } else {
            this.success = null;
            this.errors = data.message;
        }
    }

}
