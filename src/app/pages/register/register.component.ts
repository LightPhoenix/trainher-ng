import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { CustomValidators } from '../../customvalidators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    errors : string = null
    success : string = null

    registerForm = new FormGroup({
        name: new FormControl("", [Validators.required]),
        email: new FormControl("", [Validators.required, Validators.email]),
        password1: new FormControl("", [Validators.required]),
        password2: new FormControl("", [Validators.required])
    }, { validators: CustomValidators.Match('password1', 'password2') });

  constructor(private auth : AuthService) { }

  ngOnInit() {  }

  get email(){
      return this.registerForm.get("email");
  }

  get name(){
      return this.registerForm.get("name");
  }

  get password1(){
      return this.registerForm.get("password1");
  }

  get password2(){
      return this.registerForm.get("password2");
  }

  doRegister() {
      this.auth.register(this.name.value, this.email.value, this.password1.value).subscribe(
        data => this.handleRegister(data),
        error =>  console.log(error)
        );
  }

  handleRegister(data: any) {
    if(!Boolean(data.success)) {

        Object.entries(data.error).forEach(([key, value]) => {
            if(this.errors == null)
                this.errors = value + '\n';
            else
                this.errors = this.errors + value + '\n';
        });
        this.success = null;

    } else {
        this.success = data.message;
        this.errors = null;
        this.registerForm.reset();
    }
  }

}
