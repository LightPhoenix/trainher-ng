import { Component, OnInit } from '@angular/core';
import { CalendarService } from '../../services/calendar.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  calendarList = []
  month : string = ''

  prevYear : string = ''
  prevMonth : string = ''
  nextYear : string = ''
  nextMonth : string = ''

  urlParams = null

  loading : boolean = true

  constructor(private calendarService : CalendarService, private route: ActivatedRoute) { console.log(calendarService) }

  ngOnInit() {
    this.route.params.subscribe( params => this.hasParams(params) );

    if(!this.urlParams.year && !this.urlParams.month){
        this.calendarService.getCalendar().subscribe(
          data => this.fillCalendar(data),
          error =>  console.log(error)
        );
    }

  }

  hasParams(params) {
      this.urlParams = params
      this.loading = true

      this.calendarService.getCalendar(this.urlParams.year, this.urlParams.month).subscribe(
        data => this.fillCalendar(data),
        error =>  console.log(error)
      );
  }

  fillCalendar(data) {
    if(data.success == true) {
      this.calendarList = data.data;
      this.month = data.month;

      this.nextMonth = data.next_month;
      this.nextYear = data.next_year;
      this.prevMonth = data.prev_month;
      this.prevYear = data.prev_year;
      this.loading = false
    }
  }

}
