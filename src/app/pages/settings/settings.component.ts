import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ZonesService } from '../../services/zones.service';
import { environment} from '../../../environments/environment';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  dropbox_connected : boolean = false;
  dropbox_url : string = null
  dropbox_url_disconnect : string = null

  ftp_zones : {} = null
  hrt_zones : {} = null

  loading : boolean = true

    errors : string = null
    success : string = null

    settingsForm = new FormGroup({
        name: new FormControl("", [Validators.required]),
        ftp: new FormControl("", [Validators.pattern('[0-9]+')]),
        heartrate: new FormControl("", [Validators.pattern('[0-9]+')]),
        email: new FormControl("", [Validators.required, Validators.email]),
    });

  constructor(private auth: AuthService, private user: UserService) { }

  ngOnInit() {
      this.auth.redirectIfNotLoggedIn();

      this.user.getUser().subscribe(
          data => this.handleUserGet(data),
          error =>  console.log(error)
        );
  }

    get email(){
        return this.settingsForm.get("email");
    }

    get name(){
        return this.settingsForm.get("name");
    }

    get ftp(){
        return this.settingsForm.get("ftp");
    }

    get heartrate(){
        return this.settingsForm.get("heartrate");
    }

    handleUserGet(data) {
        if (Boolean(data.success) == true) {
            this.dropbox_connected = Boolean(data.data.dropbox_connected);
            this.dropbox_url = data.data.dropbox_url + '?return_url=' + window.location.href;
            this.dropbox_url_disconnect = data.data.dropbox_url_disconnect;
            this.settingsForm.get('name').setValue(data.data.name);
            this.settingsForm.get('email').setValue(data.data.email);
            this.settingsForm.get('ftp').setValue(data.data.setting_ftp);
            this.settingsForm.get('heartrate').setValue(data.data.setting_heartrate);

            if (parseInt(data.data.setting_ftp) > 0)
                this.calculateFtpZones();

            if (parseInt(data.data.setting_heartrate) > 0)
                this.calculateHeartrateZones();


            this.loading = false
        }
    }

    handleUserUpdate(data) {
        if (Boolean(data.success) == true) {
            this.success = data.message;
          } else {
            this.errors = data.error;
          }
    }

  connectDropbox() {
      window.location.href = this.dropbox_url;
  }

  disconnectDropbox() {
      window.location.href = this.dropbox_url_disconnect;
  }

  calculateFtpZones() {
    this.ftp_zones = ZonesService.getFtpZones(this.ftp.value);
  }

    calculateHeartrateZones() {
        this.hrt_zones = ZonesService.getHeartrateZones(this.heartrate.value);
    }

    updateUser() {
        this.user.updateUser(this.name.value, this.email.value, this.ftp.value, this.heartrate.value).subscribe(
            data => this.handleUserUpdate(data),
            error =>  console.log(error)
            );
    }

}
