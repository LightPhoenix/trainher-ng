import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CalendarService } from '../../services/calendar.service';
import { Location } from '@angular/common';

import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

import { environment} from '../../../environments/environment';
import * as Highcharts from 'highcharts';
import { ZonesService } from '../../services/zones.service';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {

  urlParams = null
  data : any = null
  loading : boolean = true

  Highcharts = Highcharts; // required
  chartOptions = {}; // required

  HighchartsCritPwr = Highcharts; // required
  chartOptionsCritPwr = {}; // required

  HighchartsHistPwr = Highcharts; // required
  chartOptionsHistPwr = {}; // required

  HighchartsPiePwr = Highcharts; // required
  chartOptionsPiePwr = {}; // required

  HighchartsPieHrt = Highcharts; // required
  chartOptionsPieHrt = {}; // required

  zonesForUserPwr = null
  pwrPieData = []

  zonesForUserHrt = null
  hrtPieData = []

  userHeartrate = 220

  constructor(private route: ActivatedRoute, private calendarService : CalendarService, private _location: Location, private auth: AuthService, private user: UserService) { }

  ngOnInit() {
    this.route.params.subscribe( params => this.hasParams(params) );

    this.user.getUser().subscribe(
      data => this.fillUser(data),
      error =>  console.log(error)
    );

    this.setGraphTheme();
  }

  goBack() {
    this._location.back();
  }

  hasParams(params) {
    this.urlParams = params

    this.calendarService.getActivity(this.urlParams.id).subscribe(
      data => this.fillActivity(data),
      error =>  console.log(error)
    );
  }

  setMainGraph(data: any) {

    this.Highcharts = Highcharts; // required
    this.chartOptions = {
      chart: {
        zoomType: 'x'
      },
      title: {
        text: 'Ride graph'
      },
      xAxis: {
        categories: Object.values(JSON.parse(data.date_times))
      },
      tooltip: {
        valueDecimals: 2
      },
      series: [{
        data: Object.values(JSON.parse(data.line_spd)),
        lineWidth: 0.75,
        name: 'Speed'
      },
      {
        data: Object.values(JSON.parse(data.line_hrt)),
        lineWidth: 0.75,
        name: 'Heartrate'
      },
      {
        data: Object.values(JSON.parse(data.line_pwr)),
        lineWidth: 0.75,
        name: 'Power'
      },
      {
        data: Object.values(JSON.parse(data.line_cad)),
        lineWidth: 0.75,
        name: 'Cadence'
      }
    ]}; 
  }

  setCriticalPwrGraph(data: any) {

    this.HighchartsCritPwr = Highcharts; // required
    this.chartOptionsCritPwr = {
      title: {
        text: 'Critical Power'
      },
      yAxis: {
        title: {
          text: 'Watt'
        }
      },
      xAxis: {
        categories: ["2s", "3s", "5s", "10s", "30s", "1m", "2m", "5m", "10m", "20m", "1h", "2h", '3h', "4h", "5h", "6h", "7h", "8h", "9h", "10h"]
      },
      tooltip: {
        formatter: function() {
          return Math.round(this.y) + ' watt for ' + this.x;
        },
        valueDecimals: 0
      },
      series: [{
        data: Object.values(JSON.parse(data.pwr_critical)),
        lineWidth: 0.75,
        name: 'Power'
      },
    ]};
  }

  setPiePwrGraph(data: any) {

    var values = Object.values(JSON.parse(data.pwr_part))
    
    var total = 0;
    values.forEach(obj => { total = total + (obj as number) });

    this.zonesForUserPwr = ZonesService.getFtpZones(data.pwr_ftp);

    for (var i = 0; i < values.length; i++) {

      var name = this.zonesForUserPwr[i].description;
      var description = this.zonesForUserPwr[i].low + ' - ' + this.zonesForUserPwr[i].high + ' watt';
      var value = ((values[i] as number) / total) * 100;
      
      if(i == values.length-1)
        description = this.zonesForUserPwr[i].low + '+ watt';

      var tmp = {
        name: name,
        description: description,
        y: value
      };

      this.pwrPieData.push(tmp);
    }

    this.HighchartsPiePwr = Highcharts; // required
    this.chartOptionsPiePwr = {
      title: {
        text: 'Power in zones'
      },
      chart: {
        type: 'pie'
      },
    
      tooltip: {
        headerFormat: '<span style="font-size:11px">{point.drilldown}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
      },

      series: [{ 
      dataLabels: {
        enabled: true,
        format: '{point.name}: {point.y:.1f}%'
      },
      data: this.pwrPieData    }]
      
    };
  }

  getPieHrtData(data: any) {
    var values = Object.values(JSON.parse(data.hrt_part))
    
    var total = 0;
    values.forEach(obj => { total = total + (obj as number) });

    this.zonesForUserHrt = ZonesService.getHeartrateZones(this.userHeartrate);

    for (var i = 0; i < values.length; i++) {

      var name = this.zonesForUserHrt[i].description;
      var description = this.zonesForUserHrt[i].low + ' - ' + this.zonesForUserHrt[i].high + ' bpm';
      var value = ((values[i] as number) / total) * 100;
      
      var tmp = {
        name: name,
        description: description,
        y: value
      };

      this.hrtPieData.push(tmp);
    }
  }

  setPieHrtGraph(data: any) {

    this.getPieHrtData(data);

    this.HighchartsPieHrt = Highcharts; // required
    this.chartOptionsPieHrt = {
      title: {
        text: 'Heartrate in zones'
      },
      chart: {
        type: 'pie'
      },
    
      tooltip: {
        headerFormat: '<span style="font-size:11px">{point.drilldown}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
      },

      series: [{ 
      dataLabels: {
        enabled: true,
        format: '{point.name}: {point.y:.1f}%'
      },
      data: this.hrtPieData    }]
      
    };
  }

  setHistPwrGraph(data: any) {
    var dataFormatted = []

    Object.values(JSON.parse(data.pwr_histogram)).forEach(obj => {
        dataFormatted.push(obj as number / 60)
    });
    
    this.HighchartsHistPwr = Highcharts; // required
    this.chartOptionsHistPwr = {
      title: {
        text: 'Power Histogram'
      },
      chart: {
        type: 'column'
      },
      xAxis: {
        labels: {
          formatter: function () {
              return this.axis.defaultLabelFormatter.call(this) + 'w';
          }            
        },
        categories: Object.keys(JSON.parse(data.pwr_histogram)),
      },
      yAxis: {
        labels: {
          formatter: function () {
              return this.axis.defaultLabelFormatter.call(this) + 'm';
          }            
        },
        title: {
          text: 'Time'
        }
      },
      tooltip: {
        formatter: function() {
          return this.x + ' watt for ' + Math.round(this.y) + ' minutes';
        },
        valueDecimals: 0
      },
      series: [{
        data: dataFormatted,
        name: 'Minutes'
      },
    ]};
  }

  fillActivity(data) {
    this.loading = false
      if(data.success) {
        this.data = data.data;

        this.setMainGraph(this.data);
        this.setCriticalPwrGraph(this.data);
        this.setHistPwrGraph(this.data);

        if(this.data.pwr_part)
          this.setPiePwrGraph(this.data);  

        if(this.data.hrt_part)
          this.setPieHrtGraph(this.data);   
      }
  }

  fillUser(data) {
    if(data.success == true) {
      this.userHeartrate = data.data.setting_heartrate;
      this.zonesForUserHrt = ZonesService.getHeartrateZones(this.userHeartrate);
      
      if(data.hrt_part)
        this.getPieHrtData(this.data);
    }
  }

  setGraphTheme() {
    Highcharts.createElement('link', {
      href: 'https://fonts.googleapis.com/css?family=Unica+One',
      rel: 'stylesheet',
      type: 'text/css'
    }, null, document.getElementsByTagName('head')[0]);

    Highcharts.theme = {
      colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
          '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
      chart: {
          backgroundColor: {
              linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
              stops: [
                  [0, '#2a2a2b'],
                  [1, '#3e3e40']
              ]
          },
          style: {
              fontFamily: '\'Unica One\', sans-serif'
          },
          plotBorderColor: '#606063'
      },
      title: {
          style: {
              color: '#E0E0E3',
              textTransform: 'uppercase',
              fontSize: '20px'
          }
      },
      subtitle: {
          style: {
              color: '#E0E0E3',
              textTransform: 'uppercase'
          }
      },
      xAxis: {
          gridLineColor: '#707073',
          labels: {
              style: {
                  color: '#E0E0E3'
              }
          },
          lineColor: '#707073',
          minorGridLineColor: '#505053',
          tickColor: '#707073',
          title: {
              style: {
                  color: '#A0A0A3'
  
              }
          }
      },
      yAxis: {
          gridLineColor: '#707073',
          labels: {
              style: {
                  color: '#E0E0E3'
              }
          },
          lineColor: '#707073',
          minorGridLineColor: '#505053',
          tickColor: '#707073',
          tickWidth: 1,
          title: {
              style: {
                  color: '#A0A0A3'
              }
          }
      },
      tooltip: {
          backgroundColor: 'rgba(0, 0, 0, 0.85)',
          style: {
              color: '#F0F0F0'
          }
      },
      plotOptions: {
          series: {
              dataLabels: {
                  color: '#B0B0B3'
              },
              marker: {
                  lineColor: '#333'
              }
          },
          boxplot: {
              fillColor: '#505053'
          },
          candlestick: {
              lineColor: 'white'
          },
          errorbar: {
              color: 'white'
          }
      },
      legend: {
          itemStyle: {
              color: '#E0E0E3'
          },
          itemHoverStyle: {
              color: '#FFF'
          },
          itemHiddenStyle: {
              color: '#606063'
          }
      },
      credits: {
          style: {
              color: '#666'
          }
      },
      labels: {
          style: {
              color: '#707073'
          }
      },
  
      drilldown: {
          activeAxisLabelStyle: {
              color: '#F0F0F3'
          },
          activeDataLabelStyle: {
              color: '#F0F0F3'
          }
      },
  
      navigation: {
          buttonOptions: {
              symbolStroke: '#DDDDDD',
              theme: {
                  fill: '#505053'
              }
          }
      },
  
      // scroll charts
      rangeSelector: {
          buttonTheme: {
              fill: '#505053',
              stroke: '#000000',
              style: {
                  color: '#CCC'
              },
              states: {
                  hover: {
                      fill: '#707073',
                      stroke: '#000000',
                      style: {
                          color: 'white'
                      }
                  },
                  select: {
                      fill: '#000003',
                      stroke: '#000000',
                      style: {
                          color: 'white'
                      }
                  }
              }
          },
          inputBoxBorderColor: '#505053',
          inputStyle: {
              backgroundColor: '#333',
              color: 'silver'
          },
          labelStyle: {
              color: 'silver'
          }
      },
  
      navigator: {
          handles: {
              backgroundColor: '#666',
              borderColor: '#AAA'
          },
          outlineColor: '#CCC',
          maskFill: 'rgba(255,255,255,0.1)',
          series: {
              color: '#7798BF',
              lineColor: '#A6C7ED'
          },
          xAxis: {
              gridLineColor: '#505053'
          }
      },
  
      scrollbar: {
          barBackgroundColor: '#808083',
          barBorderColor: '#808083',
          buttonArrowColor: '#CCC',
          buttonBackgroundColor: '#606063',
          buttonBorderColor: '#606063',
          rifleColor: '#FFF',
          trackBackgroundColor: '#404043',
          trackBorderColor: '#404043'
      },
  
      // special colors for some of the
      legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
      background2: '#505053',
      dataLabelsColor: '#B0B0B3',
      textColor: '#C0C0C0',
      contrastTextColor: '#F0F0F3',
      maskColor: 'rgba(255,255,255,0.3)'
    };
  
    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);
  }

}
