import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { CalendarComponent } from './pages/calendar/calendar.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { ActivityComponent } from './pages/activity/activity.component';

const routes: Routes = [
    {
      path: '',
      component: HomeComponent
    },
    {
      path: 'register',
      component: RegisterComponent
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'calendar/:year/:month',
      component: CalendarComponent
    },
    {
      path: 'calendar',
      component: CalendarComponent
    },
    {
      path: 'settings',
      component: SettingsComponent
    },
    {
      path: 'activity/:id',
      component: ActivityComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
