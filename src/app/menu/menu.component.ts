import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  isLoggedIn : boolean = false

  constructor(private auth : AuthService, private router: Router) {
    this.isLoggedIn = auth.isLoggedIn

      router.events.subscribe((val) => {
          this.auth.checkStatus()
          this.isLoggedIn = auth.isLoggedIn
      });
  }

  ngOnInit() {

    this.auth.logoutOnExpirationToken();
      setInterval(()=> {
        this.auth.logoutOnExpirationToken();
          },1000);

  }

  logout() {
    this.auth.logout();
  }

}
