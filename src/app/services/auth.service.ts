import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';
import { BaseService } from './base.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService extends BaseService {

    public isLoggedIn: boolean = (localStorage.getItem('AuthToken') != undefined) ? true : false;

    constructor(private http: HttpClient, private router: Router) {
        super();
        this.isLoggedIn = (localStorage.getItem('AuthToken') != undefined) ? true : false;
    }

    public checkStatus() {
        this.isLoggedIn = (localStorage.getItem('AuthToken') != undefined) ? true : false;
    }

    public logoutOnExpirationToken() {
        var timeStamp = Math.floor(Date.now());
        var savedTimeStamp = parseInt(localStorage.getItem('AuthTokenTimestamp'));

        if((timeStamp - savedTimeStamp) > environment.token_expire)
            this.logout();
    }

    public setToken(token: string) {
        localStorage.setItem('AuthToken', token);

        var timeStamp = Math.floor(Date.now());
        localStorage.setItem('AuthTokenTimestamp', "" + timeStamp);
    }

    public getToken() {
        return localStorage.getItem('AuthToken');
    }

    public redirectIfNotLoggedIn() {
        this.isLoggedIn = (localStorage.getItem('AuthToken') != undefined) ? true : false;

        if(!this.isLoggedIn)
            this.router.navigate(['/login']);
    }

    public redirectAfterLogin() {
        this.router.navigate(['/']);
    }

    public logout() {
        localStorage.removeItem('AuthToken');
        localStorage.removeItem('AuthTokenTimestamp');
        this.router.navigate(['/login']);
    }

    public login(email: string, password: string) {
        let url = environment.url_login;

        var body = {
            email: email,
            password: password
        };

        return this.http.post(url, body).pipe(
            map(this.extractData),
            catchError(this.handleError)
          );
    }

    public register(name: string, email: string, password: string) {
        let url = environment.url_register;

        var body = {
            name: name,
            email: email,
            password: password
        };

        return this.http.post(url, body).pipe(
            map(this.extractData),
            catchError(this.handleError)
          );
    }


}
