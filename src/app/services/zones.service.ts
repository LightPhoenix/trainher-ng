import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ZonesService {

  constructor() { }


  static getFtpZones(ftp : number) : { }
  {
    return [
            {
                name: "Zone 1",
                description: "Active recovery",
                low: 0,
                high: Math.round(ftp * 0.56),
                percentage: '56 %'
            },
            {
                name: "Zone 2",
                description: "Endurance",
                low: Math.round(ftp * 0.56) + 1,
                high: Math.round(ftp * 0.76),
                percentage: '76 %'
            },
            {
                name: "Zone 3",
                description: "Tempo",
                low: Math.round(ftp * 0.76) + 1,
                high: Math.round(ftp * 0.91),
                percentage: '91 %'
            },
            {
                name: "Zone 4",
                description: "Lactate threshold",
                low: Math.round(ftp * 0.91) + 1,
                high: Math.round(ftp * 1.06),
                percentage: '106 %'
            },
            {
                name: "Zone 5",
                description: "VO2 max",
                low: Math.round(ftp * 1.06) + 1,
                high: Math.round(ftp * 1.21),
                percentage: '121 %'
            },
            {
                name: "Zone 6",
                description: "Anaerobic Capacity",
                low: Math.round(ftp * 1.21) + 1,
                high: 2000,
                percentage: 'max'
            }
          ];
  }

    static getHeartrateZones(max_heartrate : number) : { }
    {
        return [
            {
                name: "Zone 1",
                description: "Recovery",
                low: 0,
                high: Math.round(max_heartrate * 0.647),
                percentage: 65
            },
            {
                name: "Zone 2",
                description: "Endurance",
                low: Math.round(max_heartrate * 0.647) + 1,
                high: Math.round(max_heartrate * 0.747),
                percentage: 75
            },
            {
                name: "Zone 3",
                description: "Aerobic capacity (tempo)",
                low: Math.round(max_heartrate * 0.747) + 1,
                high: Math.round(max_heartrate * 0.847),
                percentage: 85
            },
            {
                name: "Zone 4",
                description: "Lactate threshold",
                low: Math.round(max_heartrate * 0.847) + 1,
                high: Math.round(max_heartrate * 0.947),
                percentage: 95
            },
            {
                name: "Zone 5",
                description: "VO2 max",
                low: Math.round(max_heartrate * 0.947) + 1,
                high: Math.round(max_heartrate),
                percentage: 100
            }
        ];
    }

}
