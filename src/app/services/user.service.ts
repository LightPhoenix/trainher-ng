import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import { AuthService } from './auth.service';
import { BaseService } from './base.service';
import { map, catchError } from 'rxjs/operators';
import { Observable } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {

    constructor(private http: HttpClient, private auth: AuthService) { 
        super();
    }

    public getUser(): Observable<{}> {
        let url = environment.url_user_info + this.auth.getToken();
  
        return this.http.get(url).pipe(
          map(this.extractData),
          catchError(this.handleError)
        );
    }

    public updateUser(name: string, email: string, setting_ftp: number, setting_heartrate: number) {
        let url = environment.url_user_update + this.auth.getToken();

        var body = {
            name: name,
            email: email,
            setting_ftp: setting_ftp,
            setting_heartrate: setting_heartrate
        };

        return this.http.post(url, body).pipe(
            map(this.extractData),
            catchError(this.handleError)
          );
    }

}
