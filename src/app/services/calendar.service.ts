import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from '../../../node_modules/rxjs';
import { map, catchError } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class CalendarService extends BaseService {

    constructor(private http: HttpClient, private auth: AuthService) { 
      super();
    }

    public getActivity(id: string = null): Observable<{}> {
      var url = environment.url_activity.replace('<id>', id) + this.auth.getToken();

      return this.http.get(url).pipe(
        map(this.extractData),
        catchError(this.handleError)
      );
    }

    public getCalendar(year: string = null, month: string = null): Observable<{}> {
      var url = environment.url_calendar + this.auth.getToken();

      if(year && month)
        url = environment.url_calendar + this.auth.getToken() + '&year=' + year + '&month=' + month;

      return this.http.get(url).pipe(
        map(this.extractData),
        catchError(this.handleError)
      );
    }
    
}