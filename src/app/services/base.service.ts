import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';

@Injectable()

export class BaseService {

  constructor() { }

  protected extractData(res: Response) {
    let body = res;
    return body || { };
  }
  
  protected handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
